
Once the performance logger has been installed it will log the
network performance data every 30 minutes (or as configured when installing)
like magic.

All the data is written to netperf.csv,
but the data is also grouped per day and per test for convenience.

For this data to get analysed, please send back the netperf.csv file.

Once your investigation has been completed you should uninstall this 
application so that the scheduled task will be removed and no 
unnecessary resources will be wasted.
