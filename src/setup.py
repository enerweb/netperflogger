from cx_Freeze import setup, Executable
import os, sys

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [],
    includes = ['ServiceHandler', 'cx_Logging'] #,
#     buildDir = "../target",
    #include_files = ['../install/netperflogger.ini']
    )

executables = [
    Executable('netperflogger.py',
    base='Win32GUI',
    #base='Console',  causes a console window to pop up
#          path = ['src'],
        targetName = 'netperflogger.exe' #,
        #targetDir = "target" #,
        #compress = True,
        #copyDependentFiles = True,
        #appendScriptToExe = True #,
        #appendScriptToLibrary = False,
        #icon = None
    )
#   ,
#     Executable('Config.py', base='Win32Service',
#         targetName = 'netperflogger_service.exe')
#     #Executable('netperflogger.py', base='Win32Service', targetName = 'netperflogger')
]


setup(name='netperflogger',
      version = '1.1',
      description = 'Logs network performance',
      options = dict(build_exe = buildOptions),
      executables = executables
)
