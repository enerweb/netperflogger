#!/usr/bin/env python

__author__ = "Marius Kruger (amanic@gmail.com)"
_script_version = (1, 1, 1)

import ConfigParser, getopt, os, re
import signal, shlex, smtplib, socket, subprocess, sys, time, urllib, zipfile

QUIET = False
DEFAULT_TIMEOUT = 15
PING_ITERATIONS = 5
DOWNLOAD_ITERATIONS = 3
PING_TIME_MS_PATTERN = re.compile(r"(?s).*time[<=]([\.\d]+) ?ms.*")
supported_commands = ('ping', 'download', 'log', 'logloop', 'info', 'email')

DEFAULT_BASIC_CONF_FILE_NAMES = ['netperflogger']
if not getattr(sys, 'frozen', False):
    # unfrozen
    DEFAULT_BASIC_CONF_FILE_NAMES.append(
        os.path.splitext(os.path.basename(__file__))[0])

logdir = '/tmp'

def usage(out=sys.stdout):
    print >> out, ("Usage: %s [-h] [--help] [-v] [--verbose] [-q] [--quiet] "
           "[-c config_file] "
           "[--conf config_file] [location_name ...] "
           "{%s}" % (os.path.basename(sys.argv[0]),
           '|'.join(supported_commands)))

def help(out=sys.stdout):
    usage(out)
    print >> out, """
This program can be used to monitor network performance to configured
destinations.
Supported operations are: %(commands)s

You can configure it using one of the following files:
%(possibleConfFiles)s

You can use environment variables in the config file using python syntax eg:
%%(HOME)s
You can even set and use your own variables in a [DEFAULT] section.

Config file sample:
--
[DEFAULT]
logdir = /tmp
myscripthome = %%(HOME)s/bin
mydefault_timeout = 5
sleep_time_min=30
number_iterations=1440

[mylocation]
hostname=
download_urls=

""" % {'commands':', '.join(supported_commands),
    'possibleConfFiles':'\n'.join(possibleConfFiles()),
    'timeout':DEFAULT_TIMEOUT,
    }

def wrap(s, wrapper="  # %s"):
    return ''.join([wrapper % l for l in s.splitlines(True)])

#import fcntl #not supported on windows
def setNonBlocking(fd):
    """Make a fd non-blocking. can also be achieved with the python -u option"""
    '''flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags | os.O_NONBLOCK
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)'''

class Location:
    type = None

    def __init__(self):
        self.location_name = 'nameless'
        self.verbose=False
        self.failure_log = ''
        self.timeout = DEFAULT_TIMEOUT
        self._out = sys.stdout
        self.seconds_waited = 0
        self.logdir = '/tmp'
        self.hostname = 'localhost'
        self.download_urls = []

    def __str__(self):
        return self.location_name

    def __repr__(self):
        return self.location_name

    def _wait(self, isBusy, timeout=None, progress_indicator='.'):
        if timeout is None:
            timeout = self.timeout
        sleep_time = 0.0000001
        seconds_waited = 0
        while isBusy() and seconds_waited < timeout:
            time.sleep(sleep_time)
            seconds_waited += sleep_time
        self.seconds_waited += seconds_waited

    def exec_system_command(self, args, log_failures=True):
        if isinstance(args, str):
            args = shlex.split(args)
        cmd = ' '.join(args)
        if self.verbose:
            #print >> self._out, "\n  $ %s" % cmd
            self.print_info("\n  $ %s" % cmd)

        creationflags=0
        if not isPosix():
            creationflags=subprocess.SW_HIDE  #only works on windows

        p = subprocess.Popen(args, #shell=False,
        shell=True,
        creationflags=creationflags,
              stdin=subprocess.PIPE,
              stdout=subprocess.PIPE, stderr=subprocess.PIPE,
              #close_fds=True #not supported on windows
            )
        self.last_pid = p.pid
        def isBusy():
            self.last_exit_status = p.poll()
            return self.last_exit_status == None

        self._wait(isBusy)
        self.last_out = None
        self.last_err = None
        setNonBlocking(p.stdout)
        setNonBlocking(p.stderr)
        try:
            self.last_out = p.stdout.read(4096).strip()
            self.last_err = p.stderr.read(4096).strip()
            p.stdout.close()
            p.stderr.close()
            p.stdin.close()
        except IOError, e:
            self.print_err(e)
        if self.verbose and self.last_err:
            # TODO: write the process to a different file like object
            #print >> self._out, wrap(self.last_err)
            print wrap(self.last_err)
        if self.verbose and self.last_out:
            #print >> self._out, wrap(self.last_out)
            print wrap(self.last_out)
        if self.last_exit_status != 0 and log_failures:
            self.failure_log += "  $ %s\n" % cmd
            if self.last_err:
                self.failure_log += wrap(self.last_err, wrapper="    %s"
                    ) + "\n"
            if self.last_out:
                self.failure_log += wrap(self.last_out, wrapper="    %s"
                    ) + "\n"
        try:
            self._out.flush()
        except IOError, e:
            self.print_err(e)
        return (self.last_out, self.last_err, self.last_exit_status,
            self.last_pid)

    def print_failure_log(self):
        if QUIET: return
        if self.failure_log:
            print >> self._out, ''
            print >> self._out, self.failure_log

    def print_err(self, e, out=None):
        if QUIET: return
        if out == None:
            out = self._out
        print >> self._out, "[ERROR] ", e

    def print_info(self, i, out=None):
        if QUIET: return
        if out == None:
            out = self._out
        print >> self._out, "[INFO] ", i

    def info_item(self, attr_name, formatted_value=None):
        if formatted_value is None:
            return (attr_name, self.__dict__[attr_name])
        return (attr_name, formatted_value)

    def info(self, out=None):
        if out == None:
            out = self._out
        print >> out, "\n[%s]" % self

        if self.verbose:
            items = [(k, v) for k, v in self.__dict__.items() if
                not k.startswith('_') and v]
        else:
            items = []
            items.append(self.info_item("timeout", "%s seconds" % self.timeout))
            items.append(self.info_item("hostname", self.hostname))
            items.append(self.info_item("download_urls", self.download_urls))
        items.sort()
        max_key_len = max([len(k) for k, v in items]) + 1
        for k, v in items:
            print >> out, "  %s: %s" % (k.ljust(max_key_len), v)

    def timeit(self, func, *args, **argv):
        t0 = time.time()
        ret = func(*args, **argv)
        t1 = time.time()
        execution_time = t1 - t0;
        #print "execution_time: %f" % execution_time
        return ret, execution_time

    def _ping(self):
        if isPosix():
            #linux:
            cmd = "\"ping -n -c 1 -w %(timeout)d %(hostname)s\"" % self.__dict__
        else:
            #windows:
            cmd = "ping -n 1 -w %(timeout)d %(hostname)s" % self.__dict__
        (stdout, stderr, status, pid) = self.exec_system_command(cmd)
        #print "exit status:", status
        #print "seconds_waited: %f" % self.seconds_waited
        if status == 0:
            m = PING_TIME_MS_PATTERN.match(stdout)
            if m:
                return float(m.group(1))
            else:
                self.print_err("Could not parse ping output:\n%s"% self._out)
        #print "stdout:", stdout
        return -1

    def _get_log_file_path(self, action):
        file_name = ("%s_%s_%s.csv" % (self.location_name,
            re.sub(r'\W', '', action), #sanitize action
            time.strftime("%Y-%m-%d")))
        return os.path.join(self.logdir, file_name)

    def writeLog(self, action, keys, values):
        log_file_path = self._get_log_file_path(action)
        #print "#", log_file_path
        file_existed = os.path.exists(log_file_path)
        log_file = open(log_file_path, 'a')
        if not file_existed:
            log_file.write(", ".join(keys) + "\n")

        log_file.write(", ".join(values) + "\n")
        log_file.close()

    def ping(self):
        ping_times = [self._ping() for i in range(0,PING_ITERATIONS)]
        ping_times = [pt for pt in ping_times if pt != -1]
        self.print_info("ping_times : " + ', '.join(["%.1f" % r for r in ping_times]))
        if len(ping_times) != 0:
            ping_time_ms = sum(ping_times) / len(ping_times)
        else:
            ping_time_ms = -1
        self.print_info("avg(ping_times): %0.4f ms" % ping_time_ms)

        self.writeLog('ping', ['timestamp', "hostname", "milliseconds"],
            [time.strftime("%Y/%m/%d %H:%M:%S"), self.hostname,
                str(ping_time_ms)])
        return ping_time_ms

    def _download(self, url):
        try:
            f = urllib.urlopen(url + "?nocache=true")
            content = f.read()
            bytes = len(content)
            #print content
            #print
            f.close()
            return bytes
        except IOError, e:
            print e
            return -1

    def download(self):
        ret = []
        for download_url in self.download_urls:
            filename = download_url.split('/')[-1]
            rates = []
            sizes = []
            self.print_info("")
            self.print_info("url  : " + download_url)
            for i in range(0, DOWNLOAD_ITERATIONS):
                bytes, seconds = self.timeit(self._download, download_url)
                if bytes == -1 or seconds == 0:
                    bytes_per_second = -1
                    kbps = -1
                else:
                    bytes_per_second = bytes / seconds
                    kbps = bytes_per_second / 1000
                rates.append(kbps)
                sizes.append(bytes)
            bytes = min(sizes)
            if bytes == -1 or len(rates) == 0:
                kbps = -1
            else:
                kbps = sum(rates) / len(rates)
            self.print_info( "size : %s bytes" % bytes)
            self.print_info( "kbps : " + ', '.join(["%.1f" % r for r in rates]))
            self.print_info( "avg  : %.1f kbps" % kbps)

            self.writeLog('download_%s' % filename,
                ['timestamp', "filename", "bytes", 'kbps'],
                [time.strftime("%Y/%m/%d %H:%M:%S"), filename,
                    str(bytes), str(kbps)])
            ret.append((filename, bytes, kbps))
        return ret

    def cmd(self, command):
        if command in dir(self):
            c = getattr(self, command)
            if callable(c):
                c()
                return
        self.print_err("** %s does not have a command '%s'" % (
            self, command))


def _get_log_file_path():
    return os.path.join(logdir, "netperf.csv")


def _get_log_daily_file_path():
    file_name = ("netperf_%s.csv" % (time.strftime("%Y-%m-%d")))
    return os.path.join(logdir, file_name)


def _writeLog(log_file_path, keys, values):
    file_existed = os.path.exists(log_file_path)
    heading = ", ".join(keys) + "\n"
    if file_existed:
        # check if the heading is the same
        log_file = open(log_file_path, 'r')
        old_header = log_file.readline()
        log_file.close()

        if heading != old_header:
            if not QUIET:
                print "CSV heading changed, making a new file"
            # rename current file out of the way
            for i in xrange(1, 100):
                backup_file = "%s.~%i~" % (log_file_path, i)
                if not os.path.exists(backup_file):
                    os.rename(log_file_path, backup_file)
                    break
            file_existed = False # make sure header gets written again

    log_file = open(log_file_path, 'a')
    if not file_existed:
        log_file.write(", ".join(keys) + "\n")

    log_file.write(", ".join(values) + "\n")
    log_file.close()
    if not QUIET:
        print "Wrote log to:", log_file_path


def _log(locations):
    heading = []
    row = []
    kbps_map = {}

    heading.append('timestamp')
    row.append(time.strftime("%Y/%m/%d %H:%M:%S"))
    for location in locations:
        heading.append(location.location_name + "_ping_ms")
        row.append(str(location.ping()))
        for filename, bytes, kbps in location.download():
            kb = bytes/1000.0
            fn_map = kbps_map.get(filename, {})
            fn_map[location.location_name] = (filename, bytes, kbps)
            kbps_map[filename] = fn_map

    #sort download info by filename, location
    bytes_heading = []
    bytes_row = []
    kbps_heading = []
    kbps_row = []
    keys = kbps_map.keys()
    keys.sort()
    for filename in keys:
        fn_map = kbps_map[filename]
        location_names = fn_map.keys()
        location_names.sort()
        for location_name in location_names:
            (filename, bytes, kbps) = fn_map[location_name]
            kbps_heading.append("%s_%s_kbps" % (location_name, filename))
            kbps_row.append(str(kbps))
            bytes_heading.append("%s_%s_bytes" % (location_name, filename))
            bytes_row.append(str(bytes))
    heading.extend(kbps_heading)
    row.extend(kbps_row)
    heading.extend(bytes_heading)
    row.extend(bytes_row)

    if not QUIET:
        print heading
        print row
    _writeLog(_get_log_file_path(), heading, row)
    _writeLog(_get_log_daily_file_path(), heading, row)


def _email(locations):
    import email
    from socket import getfqdn, gethostname, gethostbyname;
    hostname = getfqdn()
    #print "gethostbyname(" + hostname + ")"
    #ip = gethostbyname(hostname) # not working on vista
    from cStringIO import StringIO
    infostr = StringIO()
    for location in locations:
        location.info(infostr)

    netperf_zip_path = os.path.join(logdir, "netperf.zip")
    netperf_zip = zipfile.ZipFile(netperf_zip_path, 'w')
    for filename in os.listdir(logdir):
        #if filename.endswith(".csv"):
        if filename.endswith("netperf.csv"): #only send the main file for now
            netperf_zip.write(os.path.join(logdir, filename),
                "netperf_%s.csv" % time.strftime("%Y%m%d_%H%M%S"))
    netperf_zip.close()

    msg = email.MIMEMultipart.MIMEMultipart()
    msg['From'] = from_addr
    msg['To'] = email.Utils.COMMASPACE.join(to_addrs)
    msg['Date'] = email.Utils.formatdate(localtime=True)
    msg['Subject'] = "Network Performance from %s (%s)" % (hostname, hostname)

    msg.attach(email.MIMEText.MIMEText("For " + ", ".join(
        [str(l) for l in locations]) + "\n" +
        infostr.getvalue()))

    part = email.MIMEBase.MIMEBase('application', "octet-stream")
    part.set_payload(open(netperf_zip_path,"rb").read() )
    email.Encoders.encode_base64(part)
    part.add_header('Content-Disposition',
        'attachment; filename="%s"' % os.path.basename(netperf_zip_path))
    msg.attach(part)

    print "Message length is " + repr(len(msg.as_string()))

    server = smtplib.SMTP(smtp_host)
    #server = smtplib.SMTP_SSL(smtp_host)
    server.ehlo()
    server.set_debuglevel(1)
    server.starttls()
    if smtp_user:
        server.esmtp_features["auth"] = "LOGIN PLAIN"
        #server.starttls([keyfile[, certfile]])
        #print "server.login '%s', '%s'" % (smtp_user, smtp_pwd)
        server.login(smtp_user, smtp_pwd)
    server.sendmail(from_addr, to_addrs, msg.as_string())
    server.quit()


def isPosix():
  return os.name=='posix'


def location_command(argv):
    try:
      opts, commandargs = getopt.getopt(argv, "hvqc:",
      ["help", "verbose", "quiet", "conf="])
    except getopt.GetoptError:
      usage()
      sys.exit(1)

    verbose = None
    config_file = None
    for opt, arg in opts:
      if opt in ("-h", "--help"):
          help()
          sys.exit()
      elif opt in ("-v", "--verbose"):
          verbose = True
          argv=argv[1:]
      elif opt in ("-q", "--quiet"):
          global QUIET
          QUIET = True
          argv=argv[1:]
      elif opt in ("-c", "--conf"):
          config_file = arg
          argv=argv[2:]


    if len(argv) < 1:
        print "Not enough arguments."
        usage()
        sys.exit(1)
    else:
        locations = getConfiguredLocations(config_file)
        if len(argv) > 1:
            command = argv[-1]
            location_names = argv[:-1]
            all_location_names = [location.location_name
                for location in locations]
            for location_name in location_names:
                if location_name not in all_location_names:
                    print "Unknown location: '%s'" % location_name
            locations = [location for location in locations
                        if location.location_name in location_names]
        else:
            command = argv[0]

        if len(locations) == 0:
            print "There are no valid locations configured. Check your config."
            printCurrentConfFile()
            sys.exit(1)

        if command not in supported_commands:
            print "Invalid command: '%s'" % command
            usage()
            sys.exit(1)

        if command == 'info':
            printCurrentConfFile()

        if command == 'log':
            _log(locations)
        elif command == 'logloop':
            max_iters = number_iterations
            for x in range(1, max_iters+1):
                _log(locations)
                s = sleep_time_min*60
                print "%s/%s) sleeping for %s second(s)" % (x, max_iters, s)
                time.sleep(s)
        elif command == 'email':
            _email(locations)
        else:
            for location in locations:
                if command != 'info':
                    print "*", location
                if verbose:
                    location.verbose = verbose
                location.cmd(command)


def findConfFile():
    for confFile in possibleConfFiles():
        #print "considering", confFile
        if os.path.exists(confFile):
            return confFile
    print "Could not find a config file, the following were considered:"
    for confFile in possibleConfFiles():
        print confFile
    sys.exit(1)


def printCurrentConfFile():
    print "Config file\t\t:", findConfFile()
    print "Log dir\t\t\t:", logdir
    print "Smtp host\t\t:", smtp_host
    print "From e-mail address\t:", from_addr
    print "To e-mail addresses\t:", ', '.join(to_addrs)
    print "Sleep time minutes\t:", sleep_time_min
    print "Number of iterations\t:", number_iterations

def possibleConfFiles(basic_conf_file_names=DEFAULT_BASIC_CONF_FILE_NAMES):
    possible_homedirs = (
                    os.path.expanduser("~"),
                    os.environ.get("HOME"), os.environ.get("USERPROFILE"))
    done_basic_conf_file_names = []
    for basic_conf_file_name in basic_conf_file_names:
        #don't repeat it
        if basic_conf_file_name in done_basic_conf_file_names:
            continue
        else:
            done_basic_conf_file_names.append(basic_conf_file_name)
        if getattr(sys, 'frozen', False):
            # The application is frozen
            #print("frozen", sys.executable)
            yield os.path.join(os.path.dirname(sys.executable),
                'install', basic_conf_file_name + '.ini')
        for homedir in possible_homedirs:
            if homedir and os.path.exists(homedir):
                yield os.path.join(homedir, "." + basic_conf_file_name)
                yield os.path.join(homedir, "etc", basic_conf_file_name)
                break
        yield os.path.join("/", "etc", basic_conf_file_name)


def getConfiguredLocations(conf_file_path=None):
    if conf_file_path is None:
        conf_file_path = findConfFile()
    #print "using conf file:", conf_file_path
    configFile = file(conf_file_path)
    return parseConfigFile(configFile)


def parseConfigFile(configFile):
    configParser = ConfigParser.ConfigParser(os.environ)
    configParser.readfp(configFile)
    configFile.close()
    orig_locations = []
    global logdir, smtp_host, from_addr, to_addrs, smtp_user, smtp_pwd, sleep_time_min, number_iterations
    logdir = configParser.defaults().get('logdir', logdir)
    #print "using logdir:", logdir

    sleep_time_min = configParser.defaults().get('sleep_time_min', 1)
    number_iterations = configParser.defaults().get('number_iterations', 10)
    smtp_host = configParser.defaults().get('smtp_host', 'undefined')
    from_addr = configParser.defaults().get('from_addr', 'undefined')
    to_addrs = configParser.defaults().get('to_addrs', 'undefined').split(",")
    smtp_user = configParser.defaults().get('smtp_user', None)
    smtp_pwd = configParser.defaults().get('smtp_pwd', None)

    for section in configParser.sections():
        location = Location()
        # TODO: we can probably provide a dic of key -> type converters
        # so we can cast in one iteration
        for k, v in configParser.items(section):
            # only populate known attributes
            if hasattr(location, k):
                location.__dict__[k]=v
        location.location_name = section

        # cast some fields to the required types
        location.verbose = bool(location.verbose)
        location.timeout = float(location.timeout)
        if location.download_urls:
            location.download_urls = location.download_urls.split(',')
        else:
            location.download_urls = []

        #setup log dir for this location
        location.logdir = os.path.join(logdir, location.location_name)
        if not os.path.exists(location.logdir):
            os.makedirs(location.logdir)

        orig_locations.append(location)
    # first sort alphabetically
    orig_locations.sort(lambda x,y:cmp(x.location_name, y.location_name))
    return orig_locations

if __name__ == "__main__":
    try:
        location_command(sys.argv[1:])
    except KeyboardInterrupt:
        if not QUIET:
            print "\n  *** terminated by user. ***"
