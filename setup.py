from cx_Freeze import setup, Executable
import os, sys

newpath = os.pathsep.join(('src', os.environ["PATH"], os.pathsep.join(sys.path)))
print("newpath=", newpath)

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [],
    includes = ['ServiceHandler', 'cx_Logging'],
#     buildDir = "../target",
    include_files = ['install/netperflogger.ini'] ,
        path = newpath
    )

executables = [
    Executable('src/netperflogger.py', base='Console',
#          path = ['src'],
        targetName = 'netperflogger.exe' #,
        #targetDir = "target" #,
        #compress = True,
        #copyDependentFiles = True,
        #appendScriptToExe = True #,
        #appendScriptToLibrary = False,
        #icon = None
    )
  ,
    Executable('src/Config.py', base='Win32Service',
#     path = 'src',
        targetName = 'netperflogger_service.exe')
    #Executable('netperflogger.py', base='Win32Service', targetName = 'netperflogger')
]


setup(name='netperflogger',
      version = '1.1',
      description = 'Logs network performance',
      options = dict(build_exe = buildOptions),
      executables = executables
)
