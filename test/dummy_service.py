#!/usr/bin/env python

import getopt, os, signal, subprocess, sys, time
import dummy_process

#port = 1234
supported_commands = ('status', 'start', 'stop')

def usage(out=sys.stdout):
    print >> out, ("Usage: %s [--pre_sleep <seconds>] "
           "{%s}" % (os.path.basename(sys.argv[0]),
           '|'.join(supported_commands)))

def service_command(argv, out=sys.stdout):
    if isinstance(argv, str):
        argv = shlex.split(argv)
    try:
      opts, args = getopt.getopt(argv, "",
      ["pre_sleep="])
    except getopt.GetoptError:
      print >> out, "Invalid options: ", argv
      usage()
      return 2

    pre_sleep = 0.5 #sysv tests cant start if this is less than 0.5?!
    for opt, arg in opts:
      if opt in ("--pre_sleep"):
          pre_sleep = int(arg)

    if len(args) < 1:
        print >> out, "Not enough arguments."
        usage()
        return 2
    else:
        command = args[0]
        if command not in supported_commands:
            print >> out, "Invalid command: '%s'" % command
            usage()
            return 2

        test_dir = os.path.dirname(__file__)
        dummy_process_py = os.path.join(test_dir, "dummy_process.py")

        if pre_sleep:
            print >> out, "pre-sleeping for %s seconds" % pre_sleep
            time.sleep(pre_sleep)
        if command == 'status':
            read_pid = dummy_process.readPid()
            if read_pid != None:
                #send pseudo-signal 0, to see if the process is alive
                try:
                    os.kill(read_pid, 0)
                except OSError, e:
                    #print e
                    return 1
                else:
                    print >> out, "It is running"
                    return 0
            else:
                print >> out, ("Assume it is not running, "
                    "since the pid file does not exist.")
                return 1
        elif command == 'start':
            cmd = dummy_process_py
            print >> out, "\n  $ %s" % cmd
            p = subprocess.Popen(cmd, shell=True, close_fds=True, bufsize=20,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print >> out, "Started service"
            time.sleep(0.1)
        elif command == 'stop':
            try:
                os.kill(dummy_process.readPid(), signal.SIGTERM)
                print >> out, "Stopped service"
            except OSError, e:
                #print e
                print >> out, "Was already stopped"
                return 1
    return 0


if __name__ == "__main__":
    exit(service_command(sys.argv[1:]))
