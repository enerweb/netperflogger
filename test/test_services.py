import os, re, unittest
import cStringIO
import serviceutil
import dummy_process, tests

test_dir = os.path.dirname(__file__)
dummy_service_py = os.path.join(test_dir, "dummy_service.py")
dummy_process_py = os.path.join(test_dir, "dummy_process.py")

dummy_re = 'dummy_(service|process)'

def statusLineRE(status):
    return r'\* %s is [\.\:\!\|]*\[%s\]\n$' % (dummy_re, status)

def actionLineRE(action, status):
    return r'\* %s %s[\.\:\!\|]* \[%s\]' % (
        action, dummy_re, status)

up_re = statusLineRE('UP')
down_re = statusLineRE('DOWN')
started_re = actionLineRE('Starting', 'STARTED')
stopped_re = actionLineRE('Stopping', 'STOPPED')
stopping_re = r'\* Stopping %s.*\n' % dummy_re
terminated_re = actionLineRE('Stopping', 'TERMINATED')
could_not_stop_re = actionLineRE('Stopping', 'IT COULD NOT BE STOPPED')


class TestService(tests.ServiceUtilTest):

    def _newService(self):
        raise NotImplemented()

    def setUp(self):
        self.service = self._newService()
        self.service.verbose = True
        self.execCmd('stop', [stopping_re])
        self.service.timeout = 1

    def execCmd(self, cmd, expectedREs=[]):
        print "%s..." % cmd
        self.service._out = cStringIO.StringIO()
        serviceutil.service_command([self.service], [cmd])
        out = self.service._out.getvalue()
        print out
        self.assertContainsREs(out, expectedREs)


class TestBasicService(TestService):

    def _newService(self):
        return serviceutil.BasicService(dummy_process_py)

    def testStatus(self):
        self.execCmd('status', [down_re])

        self.execCmd('start', [started_re])
        self.execCmd('status', [up_re])

        self.execCmd('stop', [could_not_stop_re])
        # I don't seem to be able to kill this process I started from python.
        # but it seems to work fine from the commandline?!
#       # self.execCmd('start', [started_re])
#       # self.execCmd('restart', [started_re, stopped_re])
#       # self.execCmd('stop', [stopped_re])

    def testInfo(self):
        self.service._out = cStringIO.StringIO()
        self.service.verbose = False
        self.service.info()
        out_str = self.service._out.getvalue()
        #print out_str
        self.assertContainsREs(out_str, [
            r'start_command +:.+dummy_process.py',
            r'timeout +: +[\.\d]+ seconds',
            r'type +: +basic',
            ], [
            r'verbose',
            ])


class TestSysVService(TestService):

    def _newService(self):
        service = serviceutil.SysVService(dummy_service_py)
        service.pid_file = dummy_process.dummy_process_pid_file
        return service

    def testStatus(self):
        self.execCmd('status', [down_re])

        self.execCmd('start', [started_re])
        self.execCmd('status', [up_re])

        self.execCmd('stop', [stopped_re])

    def testRestart(self):
        self.execCmd('start', [started_re])
        self.execCmd('restart', [started_re, stopped_re])
        self.execCmd('stop', [stopped_re])

    def testRestartWhenStopped(self):
        self.execCmd('restart', [stopping_re, started_re])

    def testKilling(self):
        self.service.timeout = 1
        self.execCmd('start', [started_re])
        self.service.stop_sub_cmd = "--pre_sleep=5 stop"
        self.execCmd('stop', [terminated_re])

    def testInfo(self):
        self.service._out = cStringIO.StringIO()
        self.service.verbose = False
        self.service.info()
        out_str = self.service._out.getvalue()
        #print out_str
        self.assertContainsREs(out_str, [
            r'init_script +:.+dummy_service.py',
            r'timeout +: +[\.\d]+ seconds',
            r'type +: +sysv',
            ], [
            r'verbose',
            ])

    def testVerboseInfo(self):
        self.service._out = cStringIO.StringIO()
        self.service.verbose = True
        self.service.info()
        out_str = self.service._out.getvalue()
        #print out_str
        self.assertContainsREs(out_str, [
            r'init_script +:.+dummy_service.py',
            r'timeout +: +[\.\d]+',
            r'type +: +sysv',
            r'verbose +: +True',
            r'hostname +: +localhost',
            r'  pid_file +:.+dummy_process.pid',
            ], [
            r'  _out',
            ])


if __name__ == '__main__':
    unittest.main()

