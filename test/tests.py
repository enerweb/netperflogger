#!/usr/bin/env python
import sys, os, re, unittest


class ServiceUtilTest(unittest.TestCase):

    def assertContainsREs(self, str, expectedREs=[], notExpectedREs=[]):
        for expectedRE in expectedREs:
            self.assertTrue(re.search(expectedRE, str),
                "Regex '%s' was not found in:\n%s" % (expectedRE, str))
        for notExpectedRE in notExpectedREs:
            self.assertFalse(re.search(notExpectedRE, str),
                "Regex '%s' was found in:\n%s" % (notExpectedRE, str))

def regressionTest():
    path = os.path.abspath(os.path.dirname(__file__))
    files = os.listdir(path)
    test = re.compile("test_.*\.py$", re.IGNORECASE)
    files = filter(test.search, files)
    filenameToModuleName = lambda f: os.path.splitext(f)[0]
    moduleNames = map(filenameToModuleName, files)
    modules = map(__import__, moduleNames)
    load = unittest.defaultTestLoader.loadTestsFromModule
    return unittest.TestSuite(map(load, modules))

if __name__ == "__main__":
    unittest.main(defaultTest="regressionTest")

