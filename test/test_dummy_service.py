import cStringIO, shlex, unittest
import dummy_service, tests

class TestService(tests.ServiceUtilTest):

    def setUp(self):
        pass

    def execCmd(self, cmd, expectedREs=[]):
        if isinstance(cmd, str):
            cmd = shlex.split(cmd)
        print "%s..." %  ' '.join(cmd)
        out = cStringIO.StringIO()
        dummy_service.service_command(cmd, out=out)
        out_str = out.getvalue()
        print out_str
        self.assertContainsREs(out_str, expectedREs)

    def testStatus(self):
        self.execCmd('stop')
        self.execCmd('status')
        self.execCmd('start', ['Started service'])
        self.execCmd('status', ['is running'])
        self.execCmd('stop', ['Stopped'])
        self.execCmd('stop', ['Was already stopped'])

    def testPreSleep(self):
        self.execCmd('start', ['Started service'])
        self.execCmd('--pre_sleep=1 stop', ['Stopped',
            'pre-sleeping for 1 seconds'])


if __name__ == '__main__':
    unittest.main()

