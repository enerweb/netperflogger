#!/usr/bin/env python

import os, sys, time

test_dir = os.path.dirname(__file__)
dummy_process_pid_file = os.path.join(test_dir, "dummy_process.pid")

def writePid(pid):
    pid_file = file(dummy_process_pid_file, 'w')
    pid_file.write(str(pid) + "\n")
    pid_file.close()

def readPid():
    try:
        pid_file = file(dummy_process_pid_file, 'r')
        pid = int(pid_file.read().strip())
        pid_file.close()
        return pid
    except Exception, e:
        print e
        return None

def deletePid():
    os.remove(dummy_process_pid_file)


def main(argv):

    if len(argv) >= 1 and argv[0].isdigit():
        port = int(argv[0])
        print "We could start listening on port %s" % port

    pid = os.getpid()
    print "my pid:", pid
    writePid(pid)
    for i in xrange(1, 100):
        print i
        time.sleep(0.1)
        read_pid = readPid()
        if read_pid != None:
            if read_pid != pid:
                print ("Another process (%s) is running now, so I can quit."
                    % read_pid)
                exit(1)
        else:
            print "My pid file was removed, so I'll quit now."
            exit(1)
    print "Done"

if __name__ == "__main__":
    main(sys.argv[1:])
