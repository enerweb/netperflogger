import os, unittest
import cStringIO
import serviceutil


class TestConfig(unittest.TestCase):
    def assertServiceAttributes(self, service, attr_dict):
        for key, value in attr_dict.items():
            self.assertEqual(
                getattr(service, key), value, '%s is not as expected\n %s'
                % (key, service.__dict__))

    def testPossibleConfFiles(self):
        conf_files = list(serviceutil.possibleConfFiles(('x', 'y')))
        home = os.environ.get("HOME")
        #print conf_files
        self.assertTrue(conf_files[0], home + ".x")
        self.assertTrue(conf_files[1], home + "/ets/x")
        self.assertTrue(conf_files[2], "/etc/x")
        self.assertTrue(conf_files[3], home + ".y")
        self.assertTrue(conf_files[4], home + "/ets/y")
        self.assertTrue(conf_files[5], "/etc/y")

    def testParseConfigFile(self):
        configFile  = cStringIO.StringIO("""
[DEFAULT]
myscripthome = %%(HOME)s/bin
mydefault_timeout = 5

[myservice]
type=sysv
init_script=%%(myscripthome)s/myservice
port=80
timeout=%%(mydefault_timeout)s
verbose=True
dependencies=yourservice,anotherservice, yourservice

[yourservice]
type=SysV
init_script=x
dependencies=anotherservice

[anotherservice]
type=SysV
init_script=x
""")
        services = serviceutil.parseConfigFile(configFile)
        # assert that the servises are returned in the correct order
        self.assertEqual(services[0].service_name, 'anotherservice')
        self.assertEqual(services[1].service_name, 'yourservice')
        self.assertEqual(services[2].service_name, 'myservice')
        anotherservice, yourservice, myservice = tuple(services)

        home = os.environ.get("HOME")
        # test some specified values
        self.assertServiceAttributes(myservice, {'service_name' : 'myservice',
            'type' : 'sysv', 'init_script' : home + '/bin/myservice',
            'port' : 80, 'timeout' : 5, 'verbose' : True})

        # test some default values
        self.assertServiceAttributes(yourservice, {
            'service_name' : 'yourservice',
            'type' : 'sysv', 'init_script' : 'x', 'port' : None,
            'timeout' : serviceutil.DEFAULT_TIMEOUT, 'verbose' : False})

    def testUsage(self):
        # just make sure it runs without errors
        out = cStringIO.StringIO()
        serviceutil.usage(out)
        out_str = out.getvalue()
        self.assertTrue('Usage:' in out_str)

    def testHelp(self):
        # just make sure help runs without errors
        out = cStringIO.StringIO()
        serviceutil.help(out)
        out_str = out.getvalue()
        self.assertTrue(
            'This program can me used to manage miscellaneous services.'
            in out_str)


if __name__ == '__main__':
    unittest.main()

